﻿using Funda.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Funda.DataAccess
{
    /// <summary>
    /// Abstracts the Funda API to get a list of houses.
    /// </summary>
    public class HousingUnitRepository : IHousingUnitRepository
    {
        private static readonly string baseEndpoint = "http://partnerapi.funda.nl/feeds/Aanbod.svc/json/";
        private static readonly string apiKey = "005e7c1d6f6c4f9bacac16760286e3cd";
        private static readonly int throttleTimeMs = 1000;
        private HttpClient httpClient = new HttpClient();

        private async Task<FundaResponse> getResponseAsync(string url)
        {
            var response = await httpClient.GetAsync(url);
            if (!response.IsSuccessStatusCode)
            {
                // Should throw a more specific exception
                throw new Exception($"Failed to retrieve data from Funda API. Status code: {response.StatusCode}, URL: {url}");
            }
            return JsonConvert.DeserializeObject<FundaResponse>(response.Content.ReadAsStringAsync().Result);
        }

        private static string BuildUrl(string city, List<string> filters, string type = "koop", int page = 1, int pageSize = 25)
        {
            var query = $"/{city}/" + string.Join("/", filters);
            return baseEndpoint + apiKey + $"/?type={type}&zo={query}&page={page}&pagesize={pageSize}";
        }

        public async Task<List<HousingUnit>> GetAllHousingUnitsAsync(string city, List<string> filters)
        {
            LogPageNumber(1);
            var response = await getResponseAsync(BuildUrl(city, filters));
            var houses = new List<HousingUnit>();
            while (response != null)
            {
                houses.AddRange(response.Objects);
                if (response.Paging.CurrentPage == response.Paging.PageCount)
                    break;
                Thread.Sleep(throttleTimeMs);

                LogPageNumber(response.Paging.CurrentPage + 1, response.Paging.PageCount);
                var url = BuildUrl(city, filters, page: response.Paging.CurrentPage + 1);
                response = await getResponseAsync(url);
            }
            ClearConsoleLine();
            return houses;
        }

        private static void LogPageNumber(int page, int total = 0)
        {
            ClearConsoleLine();
            Console.Write($"Requesting page {page}");
            if (total > 0)
                Console.Write($" of {total}");
        }

        private static void ClearConsoleLine()
        {
            Console.Write("\r" + new string(' ', Console.WindowWidth - 1) + "\r");
        }

        public void Dispose()
        {
            httpClient.Dispose();
        }
    }
}
