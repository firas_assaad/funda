﻿using Funda.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Funda.DataAccess
{
    public interface IHousingUnitRepository : IDisposable
    {
        Task<List<HousingUnit>> GetAllHousingUnitsAsync(string city, List<string> filters);
    }
}
