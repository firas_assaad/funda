﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Funda.Models
{
    /// <summary>
    /// The Funda API response... the stuff we care about at least.
    /// </summary>
    public class FundaResponse
    {
        [JsonProperty("TotaalAantalObjecten")]
        public int TotalObjectCount { get; set; }
        public List<HousingUnit> Objects { get; set; }
        public PagingParameters Paging { get; set; }
    }
}
