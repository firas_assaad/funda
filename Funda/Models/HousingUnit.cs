﻿using Newtonsoft.Json;

namespace Funda.Models
{
    // Represents the housing object returned by the Funda API. In a real
    // application, it would obviously contain more fields, but for now I only
    // care about the realtor's info. I made an assumption that two realtors with
    // the same name are the same, even if they have different IDs.
    public class HousingUnit
    {
        [JsonProperty("MakelaarId")]
        public int RealtorId { get; set; }
        [JsonProperty("MakelaarNaam")]
        public string RealtorName { get; set; }
    }
}
