﻿using Newtonsoft.Json;

namespace Funda.Models
{
    /// <summary>
    /// Paging parameters for a Funda request
    /// </summary>
    public class PagingParameters
    {
        [JsonProperty("AantalPaginas")]
        public int PageCount { get; set; }
        [JsonProperty("HuidigePagina")]
        public int CurrentPage { get; set; }
        [JsonProperty("VolgendeUrl")]
        public string NextUrl { get; set; }
    }
}
