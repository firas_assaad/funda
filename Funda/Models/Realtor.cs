﻿using System.Collections.Generic;

namespace Funda.Models
{
    public class Realtor
    {
        public string Name { get; set; }
        public List<HousingUnit> HousingUnits { get; set; }
    }
}
