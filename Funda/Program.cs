﻿using System;
using Funda.Services;
using Funda.DataAccess;
using System.Collections.Generic;
using Funda.Models;

namespace Funda
{
    class Program
    {
        public static void PrintRealtors(IEnumerable<Realtor> realtors)
        {
            Console.WriteLine();
            foreach (var realtor in realtors)
            {
                Console.WriteLine($"{realtor.Name} - {realtor.HousingUnits.Count}");
            }
        }
        static void Main(string[] args)
        {
            // Instead of using new here, it would be better to use factories or
            // a dependency injection framework to create the needed classes
            using (var repo = new HousingUnitRepository())
            {
                var realtorService = new RealtorService(repo);
                var realtors = realtorService.GetTopRealtorsForHousesWithGardensAsync("amsterdam", 10).Result;
                Console.WriteLine("Top 10 Realtors for Houses with Garden:");
                PrintRealtors(realtors);
                Console.WriteLine();
                realtors = realtorService.GetTopRealtorsForAllHousesAsync("amsterdam", 10).Result;
                Console.WriteLine("Top 10 Realtors for All Houses:");
                PrintRealtors(realtors);
            }
            Console.Read();
        }
    }
}
