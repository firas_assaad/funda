﻿using Funda.DataAccess;
using Funda.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Funda.Services
{
    public class RealtorService
    {
        public IHousingUnitRepository HousingUnitRepo { get; set; }

        public RealtorService(IHousingUnitRepository repo)
        {
            HousingUnitRepo = repo;
        }
        private async Task<IEnumerable<Realtor>> GetTopRealtorsAsync(string city, List<string> filters, int count)
        {
            if (string.IsNullOrEmpty(city))
            {
                throw new ArgumentException($"Bad city name: {city}");
            }
            if (count < 0)
            {
                throw new ArgumentException($"Invalid realtor count: {count}");
            }
            var houses = await HousingUnitRepo.GetAllHousingUnitsAsync(city.ToLower(), filters);
            var realtors = houses.GroupBy(x => x.RealtorName);
            return realtors
                .Select(x => new Realtor() { Name = x.Key, HousingUnits = x.ToList() })
                .OrderByDescending(x => x.HousingUnits.Count)
                .Take(count);
        }
        public async Task<IEnumerable<Realtor>> GetTopRealtorsForAllHousesAsync(string city, int count)
        {
            return await GetTopRealtorsAsync(city, new List<string>(), count);
        }
        public async Task<IEnumerable<Realtor>> GetTopRealtorsForHousesWithGardensAsync(string city, int count)
        {
            var filters = new List<string>() { "tuin" };
            return await GetTopRealtorsAsync(city, filters, count);
        }
                        
                

    }
}
