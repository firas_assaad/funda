﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Funda.Services;
using Funda.DataAccess;
using Moq;
using System.Collections.Generic;
using Funda.Models;
using System.Threading.Tasks;
using System;
using System.Linq;

namespace Funda.Services.Tests
{
    /// <summary>
    /// I didn't have time to add enough tests, but this is a start.
    /// Other things to test:
    /// - API URL generation
    /// - Exceptions are thrown properly
    /// - Special edge cases
    /// - etc.
    /// </summary>
    [TestClass()]
    public class RealtorServiceTests
    {
        [TestMethod()]
        public void ReturnsRequestedNumberOfRealtors()
        {
            var service = GetService();
            int realtorCount = 5;
            var realtors = service.GetTopRealtorsForAllHousesAsync("amsterdam", realtorCount).Result;
            Assert.AreEqual(realtorCount, realtors.Count(), "Incorrect realtor count");
        }
        [TestMethod()]
        public void ReturnsCorrectAmountOfHousesForRealtors()
        {
            var service = GetService();
            var realtors = service.GetTopRealtorsForAllHousesAsync("amsterdam", 10).Result;
            int expectedHousesForGrumpy = 3;
            int expectedHousesForDoc = 1;
            Assert.AreEqual(expectedHousesForGrumpy, realtors.First().HousingUnits.Count);
            Assert.AreEqual(expectedHousesForDoc, realtors.Last().HousingUnits.Count);
        }
        [TestMethod()]
        public void ReturnsTopRealtorsInCorrectOrder()
        {
            var service = GetService();
            var realtors = service.GetTopRealtorsForAllHousesAsync("amsterdam", 10).Result;
            int prevCount = realtors.First().HousingUnits.Count();
            bool sorted = true;
            foreach (var realtor in realtors)
            {
                if (realtor.HousingUnits.Count > prevCount)
                    sorted = false;
                prevCount = realtor.HousingUnits.Count;
            }
            Assert.IsTrue(sorted, "Returned realtor list is not sorted");
            Assert.AreEqual("Grumpy", realtors.First().Name, "Unexpected first realtor name");
        }
        private RealtorService GetService()
        {
            var mock = new Mock<IHousingUnitRepository>();
            mock.Setup(foo => foo.GetAllHousingUnitsAsync("amsterdam", new List<string>())).Returns(GetTestHouses());
            return new RealtorService(mock.Object);
        }
        private async Task<List<HousingUnit>> GetTestHouses()
        {
            return await Task<List<HousingUnit>>.Factory.StartNew(() => new List<HousingUnit>()
            {
                new HousingUnit()
                {
                    RealtorName = "Happy"
                },
                new HousingUnit()
                {
                    RealtorName = "Happy"
                },
                new HousingUnit()
                {
                    RealtorName = "Grumpy"
                },
                new HousingUnit()
                {
                    RealtorName = "Grumpy"
                },
                new HousingUnit()
                {
                    RealtorName = "Grumpy"
                },
                new HousingUnit()
                {
                    RealtorName = "Dopey"
                },
                new HousingUnit()
                {
                    RealtorName = "Dopey"
                },
                new HousingUnit()
                {
                    RealtorName = "Doc"
                },
                 new HousingUnit()
                {
                    RealtorName = "Bashful"
                },
                new HousingUnit()
                {
                    RealtorName = "Bashful"
                },
            });
        }
    }
}