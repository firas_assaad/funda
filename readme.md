# Application Overview
This is a small C# console application that tackles Funda's programming assignment of finding the real estate agents with the most objects listed for sale using the Funda API. I wanted to keep it simple while also showcasing some ideas for how a more featured version of the application could be structured. The solution was created in VS2017 and targets .NET 4.5.2. It is structured as follows:

- HousingUnitRepository.cs: Data Access Layer that communicates with the Funda API to get the list of houses.
- RealtorService.cs: Represents the higher-level business logic for answering the assignment's requirements.
- Program.cs: This could be considered the presentation, retrieving the results and displaying them.
- Models/: Entities that map to the Funda API. I only exposed the fields I needed for the assignment.
- RealtorServiceTest.cs: A few rudimentary tests. Not exhaustive at all, but they do exercise the main functionality.

# Assumptions
- Realtor's name is unique. This is somewhat of a big assumption, because there is an ID I could use to uniquely identify realtors. The reason I considered the name unique is because [this agency](http://www.funda.nl/makelaars/amsterdam/24550-era-van-de-steege/) and [this one](http://www.funda.nl/makelaars/diemen/24185-era-van-de-steege/) look identical to me, with multiple office locations. If it's better to consider them different then it's easy to modify the code to achieve that.
- While I do have some sanity checks, I made some assumptions about the format of the results returned by the API. So for example I don't null-check everything or handle every possible error.
-  I decided not to force the use of unnecessary design patterns or dependency injection frameworks because it felt like an overkill for something this straightforward.
- I hardcoded the temporary API key and checked it in because I assumed it's public. Similarily, I hardcoded some other values (URL, sleep time, etc.) because it's a simple application. In a real application these could be in a config file or resolved from another service.

Thanks for taking the time to review my submission. Let me know if you have any questions. :)

